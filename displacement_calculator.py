# Displacement of Mass Calculator

import math

values = [(50, 0.1),
          (55, 0.1),
          (60, 0.1),
          (65, 0.1),
          (70, 0.1),
          (75, 0.1),
          (80, 0.1),
          (85, 0.1),
          (90, 0.1),
          (95, 0.1)]

# Constants
mass = (50, 0.1)
length = (53, 0.2)


# Calculate Distance
def calc_distance(val, m, l):
    distance_1 = l[0]/2
    distance_2 = 1 / (math.pow((2 * m[0]) / val[0], 2) - 1)
    distance_3 = math.sqrt(distance_2)
    distance = distance_1 * distance_3

    return distance


# Calculate Uncertainty
def calc_uncertainty(val, m, l, d):
    calc_center_mass_uncertainty = m[1] / val[0]
    calc_mass_uncertainty = m[1] / m[0]
    calc_length_uncertainty = l[1] / l[0]

    percent_uncertainty_d2 = 2 * (calc_center_mass_uncertainty
                                  + calc_mass_uncertainty
                                  + calc_length_uncertainty)
    uncertainty_d2 = percent_uncertainty_d2 * math.pow(d, 2)
    uncertainty = uncertainty_d2 / (2 * d)

    return uncertainty


# Calculate and Print
def calc(val, m, l):
    distance = calc_distance(val, m, l)
    uncertainty = calc_uncertainty(val, m, l, distance)
    return (distance, uncertainty)


# To-Do
for val in values:
    output = calc(val, mass, length)
    print(val[0], output)
