# Displacement of Mass Calculator

This script will automatically calculate the theoretical displacement of m<sub>3</sub> along with it's uncertainty. It makes lab 3-2 of Physics 1301W significantly easier.

---

Two equal masses are tied on the ends of a string, supported by two pulleys. A third mass (m<sub>3</sub>) is hung between the pulleys and falls a certain amount.

Using the mass of the three objects and the distance between the pulleys, we can find the theoretical displacement of the center mass.

The mass and length variables should be set to the measured mass and uncertainty of the outside masses and distance between the pulleys.

The list of values should be set to the measured mass and uncertainty of the center mass. This can contain any number of values which is useful for running multiple tests.

The output is simply text, but it would be easy to pipe the data into an array by editing the section marked *#To-Do*. The *calc* function returns a tuple of value and uncertainty.
